from setuptools import setup, find_packages

setup(
    name='refracto_user',
    version='0.0.2.3',
    description='Refracto User gRPC API Private Python package',
    url='https://kgeorge1313@bitbucket.org/refracto/python-lib_refracto_user_stack.git',
    author='George Kozyrev',
    author_email='george272713@gmail.com',
    license='unlicense',
    packages=find_packages(),
    install_requires=[
        'grpcio-tools',
        'grpcio',
        'protobuf',
        'six',
        'googleapis-common-protos'
    ],
    zip_safe=False
)
